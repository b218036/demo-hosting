import React from 'react'
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/custom-animations/cube-animation.css';
import 'react-awesome-slider/dist/styles.css'
import withAutoplay from 'react-awesome-slider/dist/autoplay';

import './div2.css'

function Div2() {
    const AutoplaySlider = withAutoplay(AwesomeSlider);

    return (
        <div className = 'div2'>
            <h1>I'm div 2</h1>
            <AutoplaySlider 
    play={true}
    cancelOnInteraction={false} // should stop playing on user interaction
    interval={2000}
  >
      <div>Sanket</div>
      <div>Puhan</div>
      <div>3</div>
      <div>4</div>
  </AutoplaySlider>
        </div>
    )
}

export default Div2
