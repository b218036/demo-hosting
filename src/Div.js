import React from 'react'
import './div.css'

function Div() {
    return (
        <div className = 'div'>
            <h1>I am div</h1>
        </div>
    )
}

export default Div
