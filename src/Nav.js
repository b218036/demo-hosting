import React from 'react'
import Scrollspy from 'react-scrollspy'
import './nav.css'

function Nav() {
    return (
        <Scrollspy className = "nav" items = {['div1','div2','div3']} currentClassName = "isActive">
            <div className = 'nav_menu'><p><a href = '#div1'>Home</a></p></div>
            <div className = 'nav_menu'><p><a href = '#div2'>Gallary</a></p></div>
            <div className = 'nav_menu'><p><a href = '#div3'>Contact</a></p></div>
            <div className = 'nav_menu'><p>More about us</p></div>
        </Scrollspy>
    )
}

export default Nav
