import React from 'react'
import './div3.css'

function Div3() {
    return (
        <div className = 'div3'>
            <h1>I'm Div3</h1>
        </div>
    )
}

export default Div3
